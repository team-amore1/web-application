import React from "react";
import { navData } from "../../layout/Data";
import { useNavigate, NavLink } from "react-router-dom";
import NavItem from "../component/NavItem";
import { useLogout } from "../../hooks/useLogout";
import { useAuthContext } from "../../hooks/useAuthContext";
import logouticon from "../svg/logout.svg";

const SideBar = () => {
  const { logout } = useLogout();
  const navigate = useNavigate();
  const { user } = useAuthContext();

  const handleLogout = (e) => {
    e.preventDefault();
    logout("user");
    navigate("/");
  };

  return (
    <sidebar user={user}>
      <u className="sidebar_list">
        {navData.map(({ svg, alt, path }) => {
          return <NavItem svg={svg} alt={alt} path={path} />;
        })}
        <li className="navitem">
          <NavLink className="navlink" onClick={handleLogout}>
            <img className="nav-icon" src={logouticon} alt="logout" />
          </NavLink>
        </li>
      </u>
    </sidebar>
  );
};

export default SideBar;
