import { useAuthContext } from "./useAuthContext";

export const useLogout = () => {
  const { dispatch } = useAuthContext();

  const logout = (userType) => {
    localStorage.removeItem(userType);
    dispatch({ type: "LOGOUT" });
  };
  return { logout };
};
