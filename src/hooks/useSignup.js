import { useNavigate } from "react-router-dom";
import { useAuthContext } from "./useAuthContext";

export const useSignup = () => {
  const { dispatch } = useAuthContext();
  const navigate = useNavigate();

  const signup = async (email, password, confirmPassword) => {
    if (password !== confirmPassword) {
      console.log("Passwords do not match");
      return;
    }

    try {
      const response = await fetch("/api/user/signup", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ email, password, confirmPassword }),
      });

      if (!response.ok) {
        const json = await response.json();
        console.log(json.error);
        return;
      }

      const json = await response.json();
      localStorage.setItem("user", JSON.stringify(json));
      dispatch({ type: "LOGIN", payload: json });
      console.log(json);
      navigate("/stage/studentprofile");
    } catch (error) {
      console.log("Error during signup:", error);
    }
  };

  return { signup };
};
