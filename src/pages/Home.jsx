import React from "react";
import "../assets/css/home.css";
import ChatContainer from "../assets/component/ChatContainer";
import TinderCard from "react-tinder-card";
import { useState } from "react";

const Home = () => {
  const characters = [
    {
      name: " ",
      url: "https://i.imgur.com/7g2BUjI_d.webp",
    },
    {
      name: " ",
      url: "https://i.imgur.com/7g2BUjI_d.webp",
    },
    {
      name: " ",
      url: "https://i.imgur.com/7g2BUjI_d.webp",
    },
    {
      name: " ",
      url: "https://i.imgur.com/7g2BUjI_d.webp",
    },
    {
      name: " ",
      url: "https://i.imgur.com/7g2BUjI_d.webp",
    },
  ];

  const [lastDirection, setLastDirection] = useState();

  const swiped = (direction, nameToDelete) => {
    console.log("removing: " + nameToDelete);
    setLastDirection(direction);
  };

  const outOfFrame = (name) => {
    console.log(name + " left the screen!");
  };

  return (
    <>
      <div className="home">
        <ChatContainer />
        <div className="swiper-container">
          <div className="card-container">
            {characters.map((character) => (
              <TinderCard
                className="swipe"
                key={character.name}
                onSwipe={(dir) => swiped(dir, character.name)}
                onCardLeftScreen={() => outOfFrame(character.name)}
              >
                <div
                  style={{ backgroundImage: "url(" + character.url + ")" }}
                  className="card"
                >
                  <h3>{character.name}</h3>
                </div>
              </TinderCard>
            ))}
          </div>
          <div className="swipe-info">
            {lastDirection ? <p>You swiped {lastDirection}</p> : <p />}
          </div>
        </div>
      </div>
      ;
    </>
  );
};

export default Home;
