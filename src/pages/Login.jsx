import { Link, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { useLogin } from "../hooks/useLogin";
import { useAuthContext } from "../hooks/useAuthContext";
import { userCredentials } from "../assets/js/LoginCredentials";
import React from "react";
import "../assets/css/login.css";
import bg from "../assets/svg/bg.svg";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  console.log(email, password);
  const navigate = useNavigate();
  const { user, dispatch } = useAuthContext();
  const { login } = useLogin();

  useEffect(() => {
    if (user) navigate("/user/home");
  }, [user]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    await login(email, password, dispatch, userCredentials);
  };

  return (
    <div className="login grid">
      <img src={bg} alt="bg" className="login_bg" />
      <div className="container grid">
        <div className="login_color"></div>
        <div className="heading_login flex">
          <h1>Welcome Back!</h1>
          <h3>PHINMA UPang Student</h3>
        </div>
        <form className="form_login flex" onSubmit={handleSubmit}>
          <h2>SIGN IN</h2>
          <div className="inputs flex">
            <label className="user">
              <input
                id="email"
                type="email"
                name="email"
                placeholder="Email"
                autoComplete="true"
                required
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
                value={email}
              />
            </label>
            <label className="pass">
              <input
                id="password"
                type="password"
                name="password"
                placeholder="Password"
                autocomplete="current-password"
                required
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
                value={password}
              />
            </label>
            <h6>Forgot Password?</h6>
            <button type="submit" value="Sign In">
              Sign In
            </button>
            <div className="signup flex">
              <h5>Not a member?</h5>
              <Link to="/stage/signup" className="signup-txt">
                <h5>Signup</h5>
              </Link>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
