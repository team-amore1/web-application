import { Link, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { useSignup } from "../hooks/useSignup";
import React from "react";
import "../assets/css/signup.css";
import couple from "../assets/svg/couple.svg";
import bg from "../assets/svg/bg.svg";

const Signup = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  console.log(email, password, confirmPassword);
  const navigate = useNavigate();
  const user = JSON.parse(localStorage.getItem("user"));
  const { signup } = useSignup();

  useEffect(() => {
    if (user) navigate("/user/home");
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();

    await signup(email, password, confirmPassword);
  };

  return (
    <div className="signup_main grid">
      <img src={bg} alt="bg" className="signup_bg" />
      <div className="container grid">
        <div className="signup_color"></div>
        <div className="heading_signup flex">
          <div>
            <h1>Create Account</h1>
            <h3>Find the match that fits you</h3>
          </div>
          <img src={couple} alt="couple" />
        </div>
        <form className="form_signup flex" onSubmit={handleSubmit}>
          <h2>SIGN UP</h2>
          <div className="inputs flex">
            <label className="user">
              <input
                id="email"
                type="email"
                name="email"
                placeholder="Email"
                autoComplete="email"
                required
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              />
            </label>
            <label className="pass">
              <input
                id="password"
                type="password"
                name="password"
                placeholder="Password"
                pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$%^&*!])[A-Za-z\d@#$%^&*!]{8,}$"
                title="Password must be at least 8 characters long and include at least one uppercase letter, one lowercase letter, one number, and one special character (@, #, $, %, ^, &, *, or !)"
                required
                value={password}
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
              />
            </label>
            <label className="confirm pass">
              <input
                id="confirmpassword"
                type="password"
                name="confirmpassword"
                placeholder="Confirm Password"
                required
                value={confirmPassword}
                onChange={(e) => {
                  setConfirmPassword(e.target.value);
                }}
              />
            </label>
            <p className="signup__terms">
              By signing you agree to our <span>Terms and Conditions</span>
            </p>
            <button type="submit">Sign Up</button>
            <div className="signup flex">
              <h5>Already a member?</h5>
              <Link to="/stage/login" className="signup-txt">
                <h5>Sign In</h5>
              </Link>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Signup;
